<?php
    session_start();

    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "noteuas";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);
    
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
        integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous">
    </script>

    <title>Note Web</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="font-weight:500;">
    <a id="link1" onclick="displayNotes()" class="navbar-brand" href="javascript:void(0)">NoteApp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a id="link2" onclick="displayKategori()" class="nav-link" href="javascript:void(0)">Kategori</a>
        </li>
        <li class="nav-item">
          <a id="link3" class="nav-link" onclick="displayArsip()" href="javascript:void(0)">Arsip</a>
        </li>
      </ul>
    </div>
  </nav>
  <?php
      $query = "SELECT note.*, kategori.`kategori` FROM note JOIN kategori ON kategori.`id` = note.`idkategori` order by note.tanggal DESC";
      $query_kategori = "select id, kategori from kategori";
      $query_arsip = "SELECT arsip.*, kategori.`kategori` FROM arsip JOIN kategori ON kategori.`id` = arsip.`idkategori`";
      $query_run = mysqli_query($connection, $query);  
      $query_run1 = mysqli_query($connection, $query_kategori);
      $query_run2 = mysqli_query($connection, $query_arsip);            
  ?>
    <div id="kategori" class="container py-4" style="display:none;">
        <div class="row">
          <div class="col-md-9">
            <h2>Kategori</h2>
          </div>
          <div class="col-md-3 text-right">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addModalKat">
              <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Kategori
            </button>
          </div>
        </div>
        <?php
        if(mysqli_num_rows($query_run1) > 0)
        {
            while($row1 = mysqli_fetch_assoc($query_run1))
            {
        ?>
        <div class="alert alert-primary" role="alert" style="display:inline-block;"><i class="fa fa-tag" aria-hidden="true"></i>
          <?php echo $row1['kategori'] ?>
        </div>
        <?php
            }
        }
        else
        {
            echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
        }
        ?>
    </div>
    <div id="notes" class="container py-4">
        <div class="row mb-5">
          <div class="col-md-9">
            <h2>Notes</h2>
          </div>
          <div class="col-md-3">
            <div class="text-right">
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addModal">
                <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Note
              </button>
            </div>
          </div>
        </div>
        
        <div class="row">
            <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while($row = mysqli_fetch_assoc($query_run))
                {
            ?>
            <div class="col-md-6 mb-5">
                <div class="card" style="width: 30rem;">
                <div class="card-header" style="font-weight:700;"><?php echo $row['judul'] ?></div>
                  <img src="images/<?php echo $row['photos']; ?>" class="card-img-top" alt="..." style="max-width: 720px;
                  max-height: 660px;background-size: cover;">
                  <div class="card-body">
                    <p class="card-text"><?php echo $row['isi']; ?></p>
                    <div class="row">
                        <div class="col-md-9">
                            <p class="card-text"><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp&nbsp<?php echo $row['tanggal']; ?></small></p>
                        </div>
                        <div class="col-md-3">
                            <p class="card-text"><small class="text-muted"><i class="fa fa-tag" aria-hidden="true"></i>&nbsp&nbsp<?php echo $row['kategori']; ?></small></p>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            <?php
                }
            }
            else
            {
                echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
            }
            ?>
        </div>
    </div>

    <div id="arsip" class="container py-4"  style="display:none;">
            <h2>Arsip</h2>
        <div class="row">
            <?php
            if(mysqli_num_rows($query_run2) > 0)
            {
                while($row = mysqli_fetch_assoc($query_run2))
                {
            ?>
            <div class="col-md-6">
                <div class="card" style="width: 30rem;">
                <div class="card-header" style="font-weight:700;"><?php echo $row['judul'] ?></div>
                  <img src="images/<?php echo $row['photos']; ?>" class="card-img-top" alt="..." style="max-width: 720px;
                  max-height: 660px;background-size: cover;">
                  <div class="card-body">
                    <p class="card-text"><?php echo $row['isi']; ?></p>
                    <div class="row">
                        <div class="col-md-9">
                            <p class="card-text"><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp&nbsp<?php echo $row['tanggal']; ?></small></p>
                        </div>
                        <div class="col-md-3">
                            <p class="card-text"><small class="text-muted"><i class="fa fa-tag" aria-hidden="true"></i>&nbsp&nbsp<?php echo $row['kategori']; ?></small></p>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            <?php
                }
            }
            else
            {
                echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
            }
            ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addModalKat" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addModalLabel">Tambah Catatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" action="core.php" enctype="multipart/form-data">
              <div class="form-group">
                <label for="formGroupExampleInput">Kategori</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Isikan nama kategori..." name="kategori" required>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <input class="btn btn-success" type="submit" name="tambahkategori" value="Add">
          </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addModalLabel">Tambah Catatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" action="core.php" enctype="multipart/form-data">
              <div class="form-group">
                <label for="formGroupExampleInput">Judul Note</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Isikan Judul..." name="judul" required>
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Isi Note</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="isi" required></textarea>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Add Photo</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="photos">
                  </div>
                </div>
                <?php $query_kategori = mysqli_query($connection, "select * from kategori"); ?>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputState">Kategori</label>
                    <select id="inputState" class="form-control" name="kategori">
                    <?php while($data = mysqli_fetch_assoc($query_kategori) ){?>

                     <option value="<?php echo $data['id']; ?>"><?php echo $data['kategori']; ?></option>

                    <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal </label>
                <div class="col-sm-10">
                  <input type="date" class="form-control" id="inputEmail3" name="tanggal">
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <input class="btn btn-success" type="submit" name="tambah" value="Add">
          </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>

  <script type="text/javascript">
    function displayKategori(){
        var kategori = document.getElementById('kategori');
        var notes = document.getElementById('notes');
        var arsip = document.getElementById('arsip');
        
        var element2 = document.getElementById("link2");
        var element3 = document.getElementById("link3")

        
        element3.classList.remove("active");
        element2.classList.add("active");


        if (kategori.style.display == 'none') 
        {
            kategori.style.display = 'block';
            notes.style.display = 'none';
            arsip.style.display = 'none';
        }
        else
        {
            notes.style.display = 'none';
            arsip.style.display = 'none';
        }
    }
  </script>

  <script type="text/javascript">
    function displayNotes(){
        var kategori = document.getElementById('kategori');
        var notes = document.getElementById('notes');
        var arsip = document.getElementById('arsip');

        var element2 = document.getElementById("link2");
        var element3 = document.getElementById("link3")


        element3.classList.remove("active");
        element2.classList.remove("active");


        if (notes.style.display == 'none') 
        {
            kategori.style.display = 'none';
            notes.style.display = 'block';
            arsip.style.display = 'none';
        }
        else
        {
            kategori.style.display = 'none';
            arsip.style.display = 'none';
        }
    }
  </script>

  <script type="text/javascript">
    function displayArsip(){
        var kategori = document.getElementById('kategori');
        var notes = document.getElementById('notes');
        var arsip = document.getElementById('arsip');

        var element2 = document.getElementById("link2");
        var element3 = document.getElementById("link3")


        element3.classList.add("active");
        element2.classList.remove("active");


        if (arsip.style.display == 'none') 
        {
            kategori.style.display = 'none';
            notes.style.display = 'none';
            arsip.style.display = 'block';
        }
        else
        {
            notes.style.display = 'none';
            kategori.style.display = 'none';
        }
    }
  </script>
  
</html>