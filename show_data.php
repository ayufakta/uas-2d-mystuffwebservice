<?php
    $DB_NAME = "noteuas";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $judul = $_POST['judul'];
        $sql = "SELECT n.id, n.judul, n.isi, n.tanggal,
        k.kategori, concat('http://192.168.43.239/noteuas/images/',photos) AS url
        FROM note n, kategori k
        WHERE n.idkategori=k.id AND n.judul like '%$judul%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_note = array();
            while($nt = mysqli_fetch_assoc($result)){
                array_push($data_note, $nt);
            }
            echo json_encode($data_note);
        }
    }
?>