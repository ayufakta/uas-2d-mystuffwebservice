-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Bulan Mei 2020 pada 20.46
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `noteuas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `arsip`
--

CREATE TABLE `arsip` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` varchar(255) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `idkategori` varchar(255) DEFAULT NULL,
  `photos` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `arsip`
--

INSERT INTO `arsip` (`id`, `judul`, `isi`, `tanggal`, `idkategori`, `photos`) VALUES
(5, 'SMP', 'Buka Bersama dengan teman SMPN 3 Kota Kediri', '12 - 5 - 2020 | 17:14', '6', 'IMG-20180610-WA0032.jpg'),
(6, 'Dance', 'Lomba dance di Kediri Town Square', '12 - 5 - 2020 | 17:24', '6', 'IMG-20170521-WA0001.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
(1, 'Hobi'),
(2, 'Keuangan'),
(3, 'Kesehatan'),
(4, 'Kuliah'),
(5, 'Penting'),
(6, 'Random');

-- --------------------------------------------------------

--
-- Struktur dari tabel `note`
--

CREATE TABLE `note` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` varchar(255) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `idkategori` varchar(255) DEFAULT NULL,
  `photos` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `note`
--

INSERT INTO `note` (`id`, `judul`, `isi`, `tanggal`, `idkategori`, `photos`) VALUES
(1, 'Wisuda SMA', 'Wisuda SMA Negeri 7 Kota Kediri', '12 - 5 - 2020 | 11:1', '5', 'DC20200512110211.png'),
(2, 'Daebak', 'Pagi hari di Jembatan pak jadi', '12 - 5 - 2020 | 16:20', '6', 'IMG-20180426-WA0017.jpg'),
(3, 'Nabung!', 'Untuk membeli beberapa kebutuhan', '12 - 5 - 2020 | 16:44', '2', 'nabung.jpg'),
(4, 'Jaga Kesehatan!', 'Minum 8 gelas sehari, olahraga 30 menit, makan sayur yang banyak', '22 - 5 - 2020 | 23:21', '3', 'sehat.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `arsip`
--
ALTER TABLE `arsip`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `note`
--
ALTER TABLE `note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
